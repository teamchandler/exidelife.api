﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class point_details
    {
        public string user_id { get; set; }
        public double txn_amount{ get; set; }
        public DateTime txn_ts{ get; set; }
        public string txn_comment   { get; set; }
        public string txn_type { get; set; }
    }
}
