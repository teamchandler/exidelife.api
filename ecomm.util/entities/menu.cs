﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class menu
    {
        public string name { get; set; }
        public string id { get; set; }
        public List<menu_item> menu_items { get; set; }
    }
}
