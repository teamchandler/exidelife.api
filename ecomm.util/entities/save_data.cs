﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class save_data
    {
        public user_shipping user_shipping_address { get; set; }
        public user_cart[] cart_list { get; set; }
    }
}
