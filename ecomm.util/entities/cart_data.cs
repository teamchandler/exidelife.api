﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class cart_data
    {
        public string cart_id{ get; set; }
        public string cart_data_json { get; set; }
        public string user_id { get; set; }
    }
}
