﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class level1_banner
    {
        public string id { get; set; }
        public string href { get; set; }
        public string link { get; set; }
    }
}
