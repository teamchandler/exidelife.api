﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class child_cart_items
    {
        public string id { get; set; }
        public string size { get; set; }
        public string sku { get; set; }
        public List<stock_list> stock { get; set; }
        public string stock_msg { get; set; }
        public int current_stock { get; set; }
        public string ord_qty { get; set; } 
    }
}
