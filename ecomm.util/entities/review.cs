﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class review
    {
        public string review_date { get; set; }
        public string review_text { get; set; }
        public string rating { get; set; }
        public string link { get; set; }
            

    }
}
