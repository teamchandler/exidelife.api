﻿
using AttributeRouting.Web.Http;
using ecomm.model.repository;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ecomm.api.Controllers
{
    public class adController : ApiController
    {

        [GET("/ad/home/banner")]
        public List<adurl> get_ad_home_banner()
        {
            ad_repository a = new ad_repository();
            return a.get_landing_page_scroller();
        }
        [GET("/ad/home/static")]
        public string get_ad_home_static()
        {
            ad_repository a = new ad_repository();
            return a.get_landing_page_static_html();
        }
        [GET("/ad/level1/banner")]
        public List<level1_banner> get_level1_banner()
        {
            ad_repository a = new ad_repository();
            return a.get_level1_banner();
        }
    }
}
